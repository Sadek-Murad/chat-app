# Verwenden Sie eine spezifische Ubuntu-Version
FROM ubuntu:focal

# Installieren Sie notwendige Pakete
RUN apt-get update && apt-get install -y curl unzip software-properties-common

# Fügen Sie den HashiCorp GPG-Schlüssel hinzu
RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -

# Fügen Sie das HashiCorp Repository hinzu
RUN apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com focal main"

# Installieren Sie Terraform
RUN apt-get update && apt-get install -y terraform

# Installieren Sie die AWS CLI
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install

# Bereinigen Sie unnötige Dateien
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Setzen Sie das Arbeitsverzeichnis
WORKDIR /workspace

# Setzen Sie das Standard-Kommando
CMD ["bash"]
